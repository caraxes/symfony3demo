# Symfony 3 Product demo project

## Installation

	composer install
	
## Configuration

- Change config for DB in app/config

- Create database named "productDatabase".

### Install Database Schema

    php bin\console doctrine:schema:update --force
