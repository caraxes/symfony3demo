<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Form\ProductForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class ProductController extends Controller
{
    /**
     * @Route("/product", name="product_list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {
        $products = $this->getDoctrine()->getRepository('AppBundle:Product')
            ->findBy(array(), array('id' => 'DESC'));

        return $this->render('product/index.html.twig', array(
            'products' => $products
        ));
    }

    /**
     * @Route("/product/sums", name="product_sums")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function productSumAction(Request $request)
    {

        $products = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->select('SUM(p.price) as sum, count(p.id) as counter, IDENTITY(p.type) as type')
            ->from('AppBundle:Product', 'p')
            ->groupBy('p.type')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_OBJECT);

        return $this->render('product/sums.html.twig', array(
            'products' => $products
        ));
    }

    /**
     * @Route("/product/create", name="product_create")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $product = new Product();

        $form = $this->createForm(ProductForm::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {

            $product->setName($form['name']->getData());
            $product->setPrice((float)$form['price']->getData());
            $product->setType($form['type']->getData());

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $this->addFlash(
                'notice',
                'Product added'
            );

            return $this->redirectToRoute('product_list');
        }

        return $this->render('product/create.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/product/edit/{id}", name="product_edit")
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, Request $request)
    {
        $product = $this->getDoctrine()->getRepository('AppBundle:Product')->findOneBy(array('id' => $id));

        $product->setName($product->getName());
        $product->setPrice($product->getPrice());
        $product->setType($product->getType());

        $form = $this->createForm(ProductForm::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $product = $em->getRepository('AppBundle:Product')->find($id);
            $product->setName($form['name']->getData());
            $product->setPrice($form['price']->getData());
            $product->setType($form['type']->getData());

            $em->flush();

            $this->addFlash(
                'notice',
                'Product added'
            );

            return $this->redirectToRoute('product_list');
        }

        return $this->render('product/edit.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/product/details/{id}", name="product_details")
     *
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction($id)
    {
        $product = $this->getDoctrine()->getRepository('AppBundle:Product')->findOneBy(array('id' => $id));

        return $this->render('product/details.html.twig', array(
            'product' => $product
        ));
    }

    /**
     * @Route("/product/delete/{id}", name="product_delete")
     *
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove( $this->getDoctrine()->getRepository('AppBundle:Product')->find($id) );
        $em->flush();

        $this->addFlash(
            'notice',
            'Product removed'
        );

        return $this->redirectToRoute('product_list');
    }
}
