<?php

namespace AppBundle\Form;

use AppBundle\Entity\ProductType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        $builder
            ->add('name', TextType::class, array('attr' => array(
                'class' => 'form-control',
                'title' => 'Enter the product name',
            )))
            ->add('price', TextType::class, array('attr' => array(
                'class' => 'form-control',
                'title' => 'Enter price',
            )))
            ->add('type', EntityType::class, array(
                'class' => ProductType::class,
                'choice_label' => 'name',
                'placeholder' => 'Choose type',
                'attr' => array(
                    'class' => 'form-control',
                    'title' => 'Select type',
                )
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Create\Update product',
                'attr' => array(
                    'class' => 'btn btn-primary',
                    'title' => 'Create product'
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Product'
        ));
    }
}
